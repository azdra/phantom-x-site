const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const FaviconsWebpackPlugin = require('favicons-webpack-plugin')

const webpack = require('webpack');
const autoprefixer = require('autoprefixer');

module.exports = {
  devServer: {
    contentBase: path.join(__dirname, 'build'),
    compress: false,
    port: 9000
  },

  watchOptions: {
    aggregateTimeout: 200,
    poll: 200,
    ignored: /node_modules/
  },

  entry: {
    default: ['./src/static/js/default.js'],
    home: ['./src/static/js/app.js'],
    // shop: ['./src/static/js/shop.js'],
    // faq: ['./src/static/js/faq.js'],
  },
  mode: 'development', // development - production
  output: {
    filename: '[contenthash].js', //contenthash
    path: path.resolve(__dirname, 'build')
  },

  plugins: [
    new CleanWebpackPlugin({
      verbose: false,
      cleanStaleWebpackAssets: false,
      protectWebpackAssets: false,
    }),

    new MiniCssExtractPlugin({
      filename: '[contenthash].css',
      chunkFilename: '[contenthash].css'
    }),

    /*...[pages].forEach(p => new HtmlWebpackPlugin({
      template: path.resolve(__dirname, ),
      filename: './index.html',
      chunks: ['home'],
      cache: false,
      inject: true,
    })),*/

    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, './src/info/index.html'),
      filename: './info/index.html',
      chunks: ['default'],
      cache: false,
      inject: true,
    }),

    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, './src/download/index.html'),
      filename: './download/index.html',
      chunks: ['default'],
      cache: false,
      inject: true,
    }),

    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, './src/troubleshoot/index.html'),
      filename: './troubleshoot/index.html',
      chunks: ['default'],
      cache: false,
      inject: true,
    }),

    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, './src/faq/shop/index.html'),
      filename: './faq/shop/index.html',
      chunks: ['default'],
      cache: false,
      inject: true,
    }),

    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, './src/faq/menu/index.html'),
      filename: './faq/menu/index.html',
      chunks: ['default'],
      cache: false,
      inject: true,
    }),

    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, './src/shop/index.html' ),
      filename: './shop/index.html',
      chunks: ['default'],
      cache: false,
      inject: true,
    }),

    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, './src/index.html'),
      filename: './index.html',
      chunks: ['home'],
      cache: false,
      inject: true,
    }),

    new FaviconsWebpackPlugin({
      logo: './src/static/img/favicon.png',
      cache: true,
      publicPath: '/static/img',
      outputPath: '/static/img',
      prefix: '/static/img',
      inject: true,
      mode: 'light', // optional can be 'webapp' or 'light' - 'webapp' by default
      devMode: 'light', // optional can be 'webapp' or 'light' - 'light' by default
      favicons: {
        appName: 'my-app',
        appDescription: 'My awesome App',
        developerName: 'Me',
        developerURL: null, // prevent retrieving from the nearest package.json
        background: '#ddd',
        theme_color: '#333',
        icons: {
          android: true,              // Create Android homescreen icon. `boolean`
          appleIcon: true,            // Create Apple touch icons. `boolean`
          appleStartup: true,         // Create Apple startup images. `boolean`
          coast: true,                // Create Opera Coast icon. `boolean`
          favicons: true,             // Create regular favicons. `boolean`
          firefox: true,              // Create Firefox OS icons. `boolean`
          opengraph: true,            // Create Facebook OpenGraph image. `boolean`
          twitter: true,              // Create Twitter Summary Card image. `boolean`
          windows: true,              // Create Windows 8 tile icons. `boolean`
          yandex: true
        }
      }
    }),

    new CopyPlugin({
      patterns: [
        { from: './src/static/img/', to: './static/img/' },
      ],
    }),

  ],

  module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env'],
            plugins: ['@babel/plugin-proposal-object-rest-spread']
          }
        }
      },
      {
        test: /\.html$/,
        loader: 'html-loader',
      },
      {
        test: /\.s[ac]ss$/i,
        exclude: /node_modules/,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
          'postcss-loader',
          'sass-loader'
        ]
      },
      {
        test: /\.(ico|jpg|jpeg|png|gif|webp|svg)(\?.*)?$/,
        // exclude: /node_modules/,
        use: {
          loader: 'file-loader',
          options: {
            name: '[name].[ext]',
            outputPath: './static/img/',
            publicPath: './static/img/'
          },
        },
      },
      {
        test: /\.(eot|otf|ttf|woff|woff2)(\?.*)?$/,
        // type: 'asset/resource',
        // exclude: /node_modules/,
        use: {
          loader: 'file-loader',
          options: {
            name: '[contenthash].[ext]',
            outputPath: './static/fonts/',
            publicPath: './static/fonts/'
          },
        },
      },
    ]
  }
};
