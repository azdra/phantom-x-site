const nav = document.querySelector('.nav-header');
const burger = document.querySelector('.nav-toggle');

burger.addEventListener('click', () => {
  nav.classList.toggle('nav-header-show');
  burger.classList.toggle('activated')
})
