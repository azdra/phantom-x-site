document.addEventListener('click', (e) => {

  const target = e.target;

  if (target.parentElement.matches('.accordion-item') && target.parentElement.classList.contains('active')) {
    return target.parentElement.classList.remove('active')
  }

  const accordion = document.getElementsByClassName('accordion-item');
  for (let accordionElement of accordion) {
    if (accordionElement.classList.contains('active')) {
      accordionElement.classList.remove('active');
    }
  }

  if (target.parentElement.matches('.accordion-item')) {
    target.parentElement.classList.toggle('active')
  }

})
