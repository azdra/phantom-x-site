export default function imageMenuChange() {
  const images = [
    '/static/img/menu_theme/city-shine.png',
    '/static/img/menu_theme/halloween.png',
    '/static/img/menu_theme/red-lightning.png',
    '/static/img/menu_theme/summerparty.png',
    '/static/img/menu_theme/tropical.png',
    '/static/img/menu_theme/yellow-lightning.png',
  ]

  const image = document.querySelector('#img-menu');
  image.setAttribute('src', images[0])

  let i = 0;
  setInterval(() => {
    i >= (images.length-1) ? i = 0 : i++
    image.setAttribute('src', images[i])
  }, 3 * 1000);
}
