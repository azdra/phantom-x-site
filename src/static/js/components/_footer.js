document.querySelector('#footer').innerHTML = `
  <div class='footer-inner'>
    <div class='footer-widgets'>
      <div class='footer-widgets-menu'>
        <a target='_blank' class='discord-link tblank-icon'>
          <i class='fab fa-discord discord-icon'></i>
        </a>
        <a href='https://vk.com/phantomcommunity' target='_blank' class='tblank-icon '>
          <i class='fab fa-vk vk-icon'></i>
        </a>
      </div>
    </div>
    <div class='footer-bottom'>
      <div class='grid-container'>
        <div class='grid-x grid-margin-x footer-main-inner'>
          <div class='cell small-12 medium-6 large-6'>
            <div class='copyright'>
                <p class="margin-0">© 2021 Copyright Phantom-X</p>
            </div>
          </div>
          <div class='cell small-12 medium-6 large-6'>
            <div class='footer-bottom-inner'>
              <div class='footer-bottom-menu'>
                <a target='_blank' class='footer-link tblank-footer discord-link'>Discord</a>
                <a href='/shop' class='footer-link'> <trans>shop</trans> </a>
                <div class='footer-link dropdown dropdown-footer'>
                  Faq
                  <div class='dropdown-menu footer-dropdown-menu'>
                    <a href='/faq/menu'> <trans>menu faq</trans> </a>
                    <a href='/faq/shop'> <trans>shop faq</trans> </a>
                  </div>
                </div>
                <div class='footer-link dropdown dropdown-footer'>
                  Menu
                  <div class='dropdown-menu footer-dropdown-menu'>
                    <a href='#'> <trans>info</trans> </a>
                    <a href='/troubleshoot'> <trans>troubleshooting</trans> </a>
                    <a href='/download'> <trans>download</trans> </a>
                    <a href='https://bigdon.gitbook.io/phantomx-docs/functions' target="_blank">
                      <span class="tblank tblank-m-1"> <trans>features</trans> </span>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
`;
