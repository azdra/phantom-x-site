const header = document.querySelector('#header');

header.innerHTML = `
<div class='home-header-content'>
  <button class='nav-toggle'>
    <span></span>
    <span></span>
    <span></span>
  </button>
  <header class='header-container'>
    <div class='center-logo'>
      <a href='/' class='home-link'>
        <img src='/static/img/px_white_logo.png' alt='Phantom-X Logo' width='50'>
      </a>
    </div>
    <div class='header-content'>
      <nav class='nav-header'>
        <div class='left-side'>
          <a target='_blank' class='header-link tblank discord-link'>Discord</a>
          <a href='/shop' class='header-link'> <trans>shop</trans> </a>
        </div>
        <div class='right-side'>
          <div class='header-link dropdown'>
            Faq
            <div class='dropdown-menu'>
              <a href='/faq/menu'> <trans>menu faq</trans> </a>
              <a href='/faq/shop'> <trans>shop faq</trans> </a>
            </div>
          </div>
          <div class='header-link dropdown'>
            Menu
            <div class='dropdown-menu'>
              <a href='/info'> <trans>info</trans> </a>
              <a href='/troubleshoot'> <trans>troubleshooting</trans> </a>
              <a href='/download'> <trans>download</trans> </a>
              <a href='https://bigdon.gitbook.io/phantomx-docs/functions' target="_blank">
                <span class="tblank tblank-m-1"> <trans>features</trans> </span>
              </a>
            </div>
          </div>
        </div>
      </nav>
    </div>
  </header>
  <div class='banner-container'>
    <div class='banner-content ${location.pathname !== '/' ? `banner-content-shop` : ''} '>
      <div class='banner-title-content'>
        ${location.pathname === '/' ? `<h1 class='banner-title'> <trans>welcome to</trans> </h1>` : ''}
        <div class='banner-subtitle banner-subtitle-shop'>
          <img src='${header.getAttribute('data-img_url')}' alt='Phantom-X Shop Logo'>
        </div>
      </div>
    </div>
  </div>
</div>
`;
