document.addEventListener('click', (e) => {

  const dropdowns = document.getElementsByClassName('dropdown-menu');
  if (e.target.matches('.dropdown') && e.target.children[0].classList.contains('dropdown-menu-show')) {
    return e.target.children[0].classList.remove('dropdown-menu-show')
  }

  for (let i = 0; i < dropdowns.length; i++) {
    const openDropdown = dropdowns[i];
    if (openDropdown.classList.contains('dropdown-menu-show')) {
      openDropdown.classList.remove('dropdown-menu-show');
    }
  }

  if (e.target.matches('.dropdown') && !e.target.children[0].classList.contains('dropdown-menu-show')) {
    return e.target.children[0].classList.add('dropdown-menu-show');
  }

})
