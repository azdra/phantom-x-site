if (!localStorage.getItem('px_lang') ) localStorage.setItem('px_lang', 'en');
const texts = document.getElementsByTagName('trans');
const t = require(`../translation/${localStorage.getItem('px_lang')}.json`);

for (let text of texts) {
  if (t[text.innerHTML]) {
    text.innerText = t[text.innerHTML];
  }
}
