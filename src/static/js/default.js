import '../scss/app.scss';
import './components/dropdown';
import './components/accordion'
import './components/_footer';
import './components/_header';
import './components/discord-link';
import './components/buger-menu';
import './translation.js';

const tagerBlank = document.getElementsByClassName('tblank');
for (let tagerBlankElement of tagerBlank) {
  tagerBlankElement.setAttribute('target', '_blank')
}

const backToTop = document.querySelector('#back-to-top');
document.addEventListener('scroll', () => {
  if (document.body.scrollTop > 40 || document.documentElement.scrollTop > 40) {
    backToTop.classList.add('show');
  } else {
    if (backToTop.classList.contains('show')) backToTop.classList.remove('show');
  }
})
backToTop.addEventListener('click', () => {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
})

document.head.innerHTML += `<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css'>`
