import './default.js';

import imageMenuChange from './components/menu_change_image';

window.addEventListener("DOMContentLoaded", () => {
  imageMenuChange()
});
