module.exports = {
  'env': {
    'browser': true,
    'es2021': true,
  },
  "ignorePatterns": [
    'node_modules/*',
    'postcss.config.js',
    '.eslintrc.js',
    'webpack.**.js',
    'build/*'
  ],
  'extends': [
    'google',
  ],
  'parserOptions': {
    'ecmaVersion': 12,
    'sourceType': 'module',
  },
  'rules': {
  },
};
