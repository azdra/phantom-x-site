# Phantom-X Website

### Technologies used:
<ul>
    <li>HTML</li>
    <li>SCSS</li>
    <li>JavaScript</li>
    <li>Webpack</li>
    <li>PostCSS</li>
    <li>ESlint</li>
    <li>StyleLint</li>
</ul>

### Installation
```
1 > git clone https://gitlab.com/baptiste.brand/phantom-x-site.git
2 > cd phantom-x-site
3 > npm install
4 > npm run serve
```

### Preview 
![img.png](img.png)
![img_1.png](img_1.png)
![img_3.png](img_3.png)
![img_4.png](img_4.png)
